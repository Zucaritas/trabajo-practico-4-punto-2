package Ferreteria;

public class Orden_Venta {

    private Herramienta_Electrica articuloElectrico;
    private Herramienta_Manual articuloManual;
    private Material_Fabricado articuloFabricado;

    public Orden_Venta(Herramienta_Electrica articulo){
        this.articuloElectrico = articulo;
    }
    public Orden_Venta(Herramienta_Manual articulo){
        this.articuloManual = articulo;
    }
    public Orden_Venta(Material_Fabricado articulo){
        this.articuloFabricado = articulo;
    }

    public Herramienta_Electrica getArticuloElectrico(){
        return articuloElectrico;
    }
    public Herramienta_Manual getArticuloManual(){
        return articuloManual;
    }
    public Material_Fabricado getArticulFabricado(){
        return articuloFabricado;
    }
    
}
