package Ferreteria;

abstract class Herramienta extends Producto {

    private String funcionalidad;

    public Herramienta(int codigo, String nombre, double precio, String funcionalidad){
        super(codigo, nombre, precio);
        this.funcionalidad = funcionalidad;
    }

    public String getFuncionalidad(){
        return funcionalidad;
    }   
}
