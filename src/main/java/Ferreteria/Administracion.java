package Ferreteria;
import java.util.ArrayList;

public class Administracion {

    ArrayList<Orden_Venta> listaDeVenta = new ArrayList<>();
    private double totalAPagarElectrico;
    private double totalAPagarManual;
    private double totalAPagarMaterial;
    private double totalAPagar;

    public Administracion(){}

    public void agregarArticuloCliente(Herramienta_Electrica articulo){
        Orden_Venta nuevaVenta = new Orden_Venta(articulo);
        listaDeVenta.add(nuevaVenta);
        totalAPagarElectrico += articulo.getPrecio();
    }
    public void agregarArticuloCliente(Herramienta_Manual articulo){
        Orden_Venta nuevaVenta = new Orden_Venta(articulo);
        listaDeVenta.add(nuevaVenta);
        totalAPagarManual += articulo.getPrecio();
    }
    public void agregarArticuloCliente(Material_Fabricado articulo){
        Orden_Venta nuevaVenta = new Orden_Venta(articulo);
        listaDeVenta.add(nuevaVenta);
        totalAPagarMaterial =+ articulo.getPrecio();
    }
    public double cerrarOrdenDeVenta(){
        totalAPagar = totalAPagarElectrico + totalAPagarManual + totalAPagarMaterial;
        return totalAPagar;
    }


}