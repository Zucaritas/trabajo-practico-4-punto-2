package Ferreteria;

public class Principal {

    public static void main( String [] args){

        Administracion nuevaAdministracion = new Administracion();
        
        Material_Fabricado nuevofabricado = new Material_Fabricado(10000000, "Ripio", 250.00);
        Material_Primo nuevoPrimo = new Material_Primo(10000001, "Madera Roble", 150.00);
        Herramienta_Electrica nuevaElectrica = new Herramienta_Electrica(20000000, "Taladro", 1800.99, "Taladra cosas", 245.45);
        Herramienta_Manual nuevaManual = new Herramienta_Manual(20000001, "Lija", 120.00, "Lija cosas");

        nuevaAdministracion.agregarArticuloCliente(nuevaElectrica);
        nuevaAdministracion.agregarArticuloCliente(nuevaManual);
        nuevaAdministracion.agregarArticuloCliente(nuevofabricado);

        System.out.println("Total a Pagar: " + nuevaAdministracion.cerrarOrdenDeVenta() + "$");

    }
   
}
