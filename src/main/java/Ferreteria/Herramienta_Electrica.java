package Ferreteria;

public class Herramienta_Electrica extends Herramienta {

    private double consumo;

    public Herramienta_Electrica(int codigo, String nombre, double precio, String funcionalidad, double consumo){        
        super(codigo, nombre, precio, funcionalidad);
        this.consumo = consumo;
    }

    public double getConsumo(){
        return consumo;
    }
}
